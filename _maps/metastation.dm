#if !defined(MAP_FILE)

        #include "map_files\arctic.dmm"

        #define MAP_FILE "arctic.dmm"

#elif !defined(MAP_OVERRIDE)

	#warn a map has already been included, ignoring MetaStation.

#endif
