/mob/living/simple_animal/hostile/ferconga
	name = "ferconga"
	desc = "RawrRawr!!"
	icon_state = "ferconga"
	icon_living = "ferconga"
	icon_dead = "ferconga_dead"
	icon_gib = "bear_gib"
	speak = list("RAWR!","Rawr!","GRR!","Growl!")
	speak_emote = list("growls", "roars")
	emote_hear = list("rawrs","grumbles","grawls")
	emote_see = list("stares ferociously", "stomps")
	speak_chance = 1
	turns_per_move = 5
	see_in_dark = 6
	meat_type = /obj/item/weapon/reagent_containers/food/snacks/bearmeat
	response_help  = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm   = "pokes the"
	stop_automated_movement_when_pulled = 0
	maxHealth = 120
	health = 120
	melee_damage_lower = 20
	melee_damage_upper = 30

	melee_damage_lower = 20
	melee_damage_upper = 30
	attacktext = "claws"
	friendly = "ferconga hugs"

	//Space bears aren't affected by atmos.
	min_oxy = 0
	max_oxy = 0
	min_tox = 0
	max_tox = 0
	min_co2 = 0
	max_co2 = 0
	min_n2 = 0
	max_n2 = 0
	minbodytemp = 0

	factions = list("russian")

/mob/living/simple_animal/hostile/ferconga/Move()
	..()
	if(stat != DEAD)
		if(loc && istype(loc,/turf/space))
			icon_state = "ferconga"
		else
			icon_state = "ferconga_dead"

/mob/living/simple_animal/hostile/ferconga/Process_Spacemove(var/check_drift = 0)
	return 1	//No drifting in space!

/mob/living/simple_animal/hostile/ferconga/FindTarget()
	. = ..()
	if(.)
		emote("stares alertly at [.]")
		stance = HOSTILE_STANCE_ATTACK

/mob/living/simple_animal/hostile/ferconga/LoseTarget()
	..(5)
























