/mob/living/simple_animal/hostile/wolf
	name = "wolf"
	desc = "RawrRawr!!"
	icon_state = "wolf"
	icon_living = "wolf"
	icon_dead = "wolf_dead"
	icon_gib = "bear_gib"
	speak = list("RAWR!","Rawr!","Bark!","Woooooo!")
	speak_emote = list("growls", "roars")
	emote_hear = list("rawrs","grumbles","grawls")
	emote_see = list("stares ferociously", "stomps")
	speak_chance = 1
	turns_per_move = 5
	see_in_dark = 6
	meat_type = /obj/item/weapon/reagent_containers/food/snacks/meat
	response_help  = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm   = "pokes the"
	stop_automated_movement_when_pulled = 0
	maxHealth = 30
	health = 30
	melee_damage_lower = 3
	melee_damage_upper = 5

	melee_damage_lower = 3
	melee_damage_upper = 5
	attacktext = "claws"
	friendly = "wolf licks"


/mob/living/simple_animal/hostile/wolf/Move()
	..()

/mob/living/simple_animal/hostile/wolf/Process_Spacemove(var/check_drift = 0)
	return 1	//No drifting in space!

/mob/living/simple_animal/hostile/wolf/FindTarget()
	. = ..()
	if(.)
		emote("stares alertly at [.]")
		stance = HOSTILE_STANCE_ATTACK

/mob/living/simple_animal/hostile/wolf/LoseTarget()
	..(5)